var indexSectionsWithContent =
{
  0: "_abcdefgilmnoprstuw",
  1: "abdlst",
  2: "cdfgilmrstu",
  3: "abefilmnopw",
  4: "ls",
  5: "ls",
  6: "blmnprst",
  7: "bdls",
  8: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Archivos",
  2: "Funciones",
  3: "Variables",
  4: "Enumeraciones",
  5: "Valores de enumeraciones",
  6: "defines",
  7: "Grupos",
  8: "Páginas"
};

