var searchData=
[
  ['tcrt5000contador',['Tcrt5000Contador',['../tcrt5000_8h.html#aea162984999b87c826b1196c6dea0cc7',1,'tcrt5000.h']]],
  ['tcrt5000cuenta',['Tcrt5000Cuenta',['../tcrt5000_8c.html#ae08f12cd62501886e67c039666084c0d',1,'tcrt5000.c']]],
  ['tcrt5000deinit',['Tcrt5000Deinit',['../tcrt5000_8c.html#af64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;tcrt5000.c'],['../tcrt5000_8h.html#af64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;tcrt5000.c']]],
  ['tcrt5000init',['Tcrt5000Init',['../tcrt5000_8c.html#ab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c'],['../tcrt5000_8h.html#ab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;tcrt5000.c']]],
  ['tcrt5000lectura',['Tcrt5000Lectura',['../tcrt5000_8c.html#a3508fce50f36dcd6b3f0819a5dd8b1c3',1,'Tcrt5000Lectura(void):&#160;tcrt5000.c'],['../tcrt5000_8h.html#afa38dcbcde92d82969519839752e4c0b',1,'Tcrt5000Lectura(void):&#160;tcrt5000.c']]],
  ['tcrt5000state',['Tcrt5000State',['../tcrt5000_8c.html#a54f67a2c9a91c4ec3c00f39fdcc1e6d5',1,'Tcrt5000State(void):&#160;tcrt5000.c'],['../tcrt5000_8h.html#a51671fa99fcdfd5bfb58feb05fca49b9',1,'Tcrt5000State(void):&#160;tcrt5000.c']]]
];
