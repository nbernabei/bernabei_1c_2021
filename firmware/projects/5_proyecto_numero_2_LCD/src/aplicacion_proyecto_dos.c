/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage TCRT5000 Detector Infrarrojo
 *
 * @section genDesc General Description
 *
 * Esta aplicación permite contar objetos, los cuales son detectados mediante un sensor infrarrojo.
 * El resultado de conteo se muestran en una pantalla LCD.
 * Además mediante botones el conteo puede reiniciarse, pausarse o mantener un resutado mientras
 * el conteo continúa.
 *
 * @section hardConn Hardware Connection
 *
 *
 * |   TRCT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND	    	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/05/2021 | Creación del documento		                         |
 * | 			| 	                     						 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "aplicacion_proyecto_dos.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "tcrt5000.h"
#include "DisplayITS_E0803.h"

/*==================[macros and definitions]=================================*/

#define LIMITE 10
/**
 * @def LIMITE 10
 * @brief Límite del contador BCD
 */

#define BIT0 0x01
/**
 * @def BIT0
 * @brief Máscara de comparación para funciones interactivas
 */

/*==================[internal data definition]===============================*/

gpio_t pins[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5}; /**< Puertos para conexión con dispositivo DisplayITS_E0803 */

bool stopper; /**< Variable para pausar o continuar el conteo*/
bool holder; /**< Variable para mantener en pantalla el resultado al momento de la interacción*/
uint16_t contador_lcd; /**< Variable que lleva la cuenta que se muestra por dispositivo LCD*/
uint8_t contador_led; /**< Variable que realiza el conteo BCD que se muestra en los leds de la EDU-CIAA*/
/*==================[internal functions declaration]=========================*/

/** @fn LedOnValorBCD
 *
 * @brief Función que transforma a BCD el último digito del valor actual del conteo.
 * Y lo muestra utilizando los leds de la EDU-CIAA
 * @param Recibe el último dígito del conteo actual
 *
 * @return No retorna ningún valor
 */
void LedOnValorBCD(uint8_t digito){

	uint8_t MASK = 1;

	//Bit 0 del digito
	if(digito&MASK){
		LedOn(LED_3);
		MASK = MASK << 1;
	} else{
		LedOff(LED_3);
		MASK = MASK << 1;
	  }
	//Bit 2 del digito
	if(digito&MASK){
		LedOn(LED_2);
		MASK = MASK << 1;
	} else{
		LedOff(LED_2);
		MASK = MASK << 1;
	  }
	//Bit 3 del digito
	if(digito&MASK){
		LedOn(LED_1);
		MASK = MASK << 1;
	} else{
		LedOff(LED_1);
		MASK = MASK << 1;
	  }
	//Bit 4 del digito
		if(digito&MASK){
			LedOn(LED_RGB_B);
			MASK = MASK << 1;
		} else{
			LedOff(LED_RGB_B);
			MASK = MASK << 1;
		  }

}

/** @fn StopCount
 *
 * @brief Función interactiva que detiene el conteo.
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
void StopCount(void){
	DelayMs(300);
	stopper= !stopper;
}

/** @fn HoldCount
 *
 * @brief Función interactiva que mantiene en pantalla el resultado del conteo al momento de oprimir.
 * Sin embargo, el conteo se continua realizando-
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
void HoldCount(void){
	DelayMs(300);
	holder= !holder;
	if(holder){
		ITSE0803DisplayValue(contador_lcd);
		LedOnValorBCD(contador_led);
	}
}

/** @fn ResetCount
 *
 * @brief Función interactiva que reinicia el conteo.
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
void ResetCount(void){
	DelayMs(300);
	contador_lcd= 0;
	contador_led= 0;
	ITSE0803DisplayValue(contador_lcd);
	LedOnValorBCD(contador_led);
	stopper= false;
	holder= false;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** @fn main
 *
 * @brief Corre el programa.
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
int main(void)
{
	//uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
//	LedOn(LED_3);
//	DelaySec(2);
//	LedOff(LED_3);

	Tcrt5000Init(GPIO_T_COL0);
	ITSE0803Init(pins);

	stopper= false;
	holder= false;
	contador_lcd= 0;
	contador_led= 0;
	ITSE0803DisplayValue(contador_lcd);
	LedOnValorBCD(contador_led);

	SwitchActivInt(SWITCH_1, StopCount);
	SwitchActivInt(SWITCH_2, HoldCount);
	SwitchActivInt(SWITCH_3, ResetCount);

	while(1){
		if(!stopper){
			if( Tcrt5000Cuenta() ){
				contador_lcd++;
				contador_led++;
				if(contador_led == LIMITE){
					contador_led= 0;
				}
				if(!holder){
					ITSE0803DisplayValue(contador_lcd);
					LedOnValorBCD(contador_led);
				}

			}
		}


	}

    return 0;
}

/*==================[end of file]============================================*/

