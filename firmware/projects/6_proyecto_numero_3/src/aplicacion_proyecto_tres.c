/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage TCRT5000 Contador Infrarrojo
 *
 * @section genDesc General Description
 *
 * Esta aplicación permite contar objetos, los cuales son detectados mediante un sensor infrarrojo.
 * El resultado de conteo se envía por USB a una PC.
 * Además mediante botones de EDU-CIAA o mediante el uso del teclado, el conteo puede reiniciarse, pausarse o mantener un resutado
 * mientras el conteo continúa.
 * Se utiliza el software "hterm" para comunicación PC/EDU-CIAA
 * @section hardConn Hardware Connection
 *
 *
 * |   TRCT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND	    	| 	GND			|
 *
 *
 * |     PC   		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	USB	 	 	| 	PORT_PC		|
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/05/2021 | Creación del documento		                         |
 * | 20/05/2021	| Actualización del documento 	                     						 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/aplicacion_proyecto_tres.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "tcrt5000.h"
#include "DisplayITS_E0803.h"
#include "timer.h"
#include "uart.h"
#include "string.h"

/*==================[macros and definitions]=================================*/

#define LIMITE 10
/**
 * @def LIMITE 10
 * @brief Límite del contador BCD.
 */

#define BIT0 0x01
/**
 * @def BIT0
 * @brief Máscara de comparación para funciones interactivas.
 */

#define MASK_O 0x4F
/**
 * @def MASK_O
 * @brief Máscara de comparación para entrada por teclado.
 */
#define MASK_o 0x6F
/**
 * @def MASK_o
 * @brief Máscara de comparación para entrada por teclado.
 */
#define MASK_H 0x48
/**
 * @def MASK_H
 * @brief Máscara de comparación para entrada por teclado.
 */
#define MASK_h 0x68
/**
 * @def MASK_h
 * @brief Máscara de comparación para entrada por teclado.
 */
#define MASK_0 0x30
/**
 * @def MASK_0
 * @brief Máscara de comparación para entrada por teclado.
 */

/*==================[internal data definition]===============================*/

void LedOnValorBCD(uint8_t digito);
void StopCount(void);
void HoldCount(void);
void ResetCount(void);
void ShowResult(void);
void USBSendResult(void);

gpio_t pins[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5}; /**< Puertos para conexión con dispositivo DisplayITS_E0803.*/

bool stopper; /**< Variable para pausar o continuar el conteo.*/
bool holder; /**< Variable para mantener en pantalla el resultado al momento de la interacción.*/
uint16_t contador_lcd; /**< Variable que lleva la cuenta que se muestra por dispositivo LCD.*/
uint8_t contador_led; /**< Variable que realiza el conteo BCD que se muestra en los leds de la EDU-CIAA.*/

char info_static []={" lineas\r\n"};/**< String que complementa a la información que se envía a la PC.*/

//timer_config my_timer={TIMER_A,1000,&ShowResult};
timer_config my_timer_usb={TIMER_A,1000,&USBSendResult};/**< Timer que dispara el envío del resultado del conteo a la PC.*/

/*==================[internal functions declaration]=========================*/

/** @fn LeerTeclado
 *
 * @brief Función que toma información de la PC y evalúa si es un comando válido.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void LeerTeclado(void)
{
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);

	if((dat==MASK_O)||(dat==MASK_o)){
		StopCount();
	}
	if((dat==MASK_H)||(dat==MASK_h)){
		HoldCount();
	}
	if(dat==MASK_0){
		ResetCount();
	}
}

/** @fn SysInit
 *
 * @brief Función que inicia todos los drivers a utilizarse en la aplicación.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SysInit(void)
{
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = LeerTeclado;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	//TimerInit(&my_timer);
	TimerInit(&my_timer_usb);
	TimerStart(TIMER_A);
	Tcrt5000Init(GPIO_T_COL0);
	ITSE0803Init(pins);
	UartInit(&UART_USB);
}

/** @fn LedOnValorBCD
 *
 * @brief Función que transforma a BCD el último digito del valor actual del conteo.
 * Y lo muestra utilizando los leds de la EDU-CIAA.
 * @param[in] Recibe el último dígito del conteo actual.
 *
 * @return No retorna ningún valor.
 */
void LedOnValorBCD(uint8_t digito){

	uint8_t MASK = 1;

	//Bit 0 del digito
	if(digito&MASK){
		LedOn(LED_3);
		MASK = MASK << 1;
	} else{
		LedOff(LED_3);
		MASK = MASK << 1;
	  }
	//Bit 2 del digito
	if(digito&MASK){
		LedOn(LED_2);
		MASK = MASK << 1;
	} else{
		LedOff(LED_2);
		MASK = MASK << 1;
	  }
	//Bit 3 del digito
	if(digito&MASK){
		LedOn(LED_1);
		MASK = MASK << 1;
	} else{
		LedOff(LED_1);
		MASK = MASK << 1;
	  }
	//Bit 4 del digito
		if(digito&MASK){
			LedOn(LED_RGB_B);
			MASK = MASK << 1;
		} else{
			LedOff(LED_RGB_B);
			MASK = MASK << 1;
		  }

}

/** @fn StopCount
 *
 * @brief Función interactiva que detiene el conteo.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void StopCount(void){
	DelayMs(300);
	stopper= !stopper;
}

/** @fn HoldCount
 *
 * @brief Función interactiva que mantiene en pantalla el resultado del conteo al momento de oprimir.
 * Sin embargo, el conteo se continua realizando.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void HoldCount(void){
	DelayMs(300);
	holder= !holder;
	if(holder){
		ITSE0803DisplayValue(contador_lcd);
		LedOnValorBCD(contador_led);
	}
}

/** @fn ResetCount
 *
 * @brief Función interactiva que reinicia el conteo.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ResetCount(void){
	DelayMs(300);
	contador_lcd= 0;
	contador_led= 0;
	ITSE0803DisplayValue(contador_lcd);
	LedOnValorBCD(contador_led);
	stopper= false;
	holder= false;
}

void ShowResult(void){
	if(!holder){
		ITSE0803DisplayValue(contador_lcd);
		LedOnValorBCD(contador_led);
	}
}

/** @fn USBSendResult
 *
 * @brief Función que envía el resultado actual del contador para mostrar por PC.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void USBSendResult(void){
	if(!holder){
		LedOnValorBCD(contador_led);
//		char info_contador[3]= {0};
//		*info_contador= *UartItoa(contador_lcd, 10);
		UartSendString(SERIAL_PORT_PC, UartItoa(contador_lcd, 10));
		UartSendString(SERIAL_PORT_PC, info_static);
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	stopper= false;
	holder= false;
	contador_lcd= 0;
	contador_led= 0;
	ITSE0803DisplayValue(contador_lcd);
	LedOnValorBCD(contador_led);

	SwitchActivInt(SWITCH_1, StopCount);
	SwitchActivInt(SWITCH_2, HoldCount);
	SwitchActivInt(SWITCH_3, ResetCount);

	while(1){
		if(!stopper){
			if( Tcrt5000Cuenta() ){
				contador_lcd++;
				contador_led++;
				if(contador_led == LIMITE){
					contador_led= 0;
				}
			}
		}
	}

    return 0;
}

/*==================[end of file]============================================*/

