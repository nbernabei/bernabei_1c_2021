/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage FronPong
 *
 * @section genDesc General Description
 *
 * Esta aplicación diseña e implementa un juego similar al Pong, dicho juego
 * se grafica por pantalla led que se encuentra conectada a la EDU-CIAA.
 * 
 * @section hardConn Hardware Connection
 * 
 * La conexión hardware de la pantalla se encuentra en el driver correspondiente.
 *
 * |   EDU-CIAA		|   POTENCIOMETRO	|
 * |:--------------:|:--------------|
 * | 	5V0 (P1)	| 	PIN IZQ		|
 * | 	CH1 (P1)	| 	PIN CEN		|
 * | 	GND (P1)	| 	PIN DER		|
 *
 * |     PC   		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	USB	 	 	| 	PORT_PC		|
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/06/2021 | Creación del documento.		                 |
 * | 19/06/2021 | Actualización del documento.		                 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "stdio.h"
#include "stdlib.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "spi.h"
#include "fonts.h"
#include "ili9341.h"
#include "analog_io.h"
#include "../inc/aplicacion_proyecto_cinco.h"       /* <= own header */

/*==================[macros and definitions]=================================*/
#define SAMPLE_SIZE 5
/**
 * @def SAMPLE_SIZE 5
 * @brief Tamaño del vector de muestras que definen la posición de la paleta.
 */
#define RADIO_BALL 3
/**
 * @def RADIO_BALL 3
 * @brief Radio de la pelota.
 */
#define WALL_THICKNESS 5
/**
 * @def WALL_THICKNESS 5
 * @brief Espesor de la pared.
 */
#define RADIO_PADDLE 10
/**
 * @def RADIO_PADDLE 10
 * @brief Radio de la paleta.
 */
#define CHANGE_SPEED_BALL 5000
/**
 * @def CHANGE_SPEED_BALL 5000
 * @brief Cantidad de milésimas que transcurren antes de aumentar la velocidad de la pelota.
 */
#define CHANGE_REFRESH_SCREEN 1000
/**
 * @def CHANGE_REFRESH_SCREEN 1000
 * @brief Cantidad de milésimas que transcurren antes de aumentar la velocidad de refresco de la pantalla.
 */
/*==================[internal data definition]===============================*/
bool start = false;/**< Bandera que marca el inicio del juego.*/
bool begin = true;/**< Bandera que marca si el juego fue iniciado.*/
bool finish = false;/**< Bandera que marca el fin del juego.*/
uint32_t rand_time = 0;/**< Variable para aleatorizar el movimiento de la pelota.*/
uint32_t game_time = 0;/**< Variable para determinar el tiempo de juego neto, además se usa para aleatorizar el movimiento de la pelota.*/

uint8_t refresh_time = 100;/**< Cantidad de iteraciones necesarias antes de refrescar la pantalla led.*/
uint8_t speed_ball = 0;/**< Variable para aumentar la velocidad de la pelota.*/

char info_start[]={"Juego iniciado.\r\n"};/**< String que complementa a la información que se envía a la PC.*/
char info_static[]={"Juego terminado. Tiempo de juego, en segundos: "};/**< String que complementa a la información que se envía a la PC.*/
char info_dot[] = {"."};/**< String que complementa a la información que se envía a la PC.*/
char end_game[]={"Game Over"};/**< String que complementa a la información que se envía a la PC.*/

uint16_t pos_paddle = 10;/**< Posición central de la paleta.*/
uint16_t pos_ball_x = 0;/**< Posición eje X de la pelota.*/
uint16_t pos_ball_y = 13;/**< Posición eje Y de la pelota.*/
uint16_t old_x = 0;/**< Ultima posición eje X de la pelota.*/
uint16_t old_y = 0;/**< Ultima posición eje Y de la pelota.*/
int8_t delta_x = 0;/**< Variación de movimiento en eje X de la pelota.*/
int8_t delta_y = 0;/**< Variación de movimiento en eje Y de la pelota.*/

uint8_t refresh = 0;/**< Cantidad de iteraciones realizadas previo refrescar la pantalla led.*/
uint16_t analog_value_input; /**< Valor que ingresa a CH1 del conversor AD.*/
uint8_t pos_samples = 0; /**< Posición que recorre el arreglo de muestras.*/
uint16_t analog_samples[SAMPLE_SIZE]={ 0 };/**< Arreglo inicial de muestras */

uint16_t CalcularPosXBall(void);
uint16_t CalcularPosYBall(void);
void DrawPaddle(void);
void DrawBall(void);
void DrawCourt(void);
void DrawInit(void);
void ReadAnalogValue(void);
void SendAnalogValue(void);
uint16_t CalcularPosPaddle(void);
void StartGame(void);
void ResetGame(void);

analog_input_config my_analog={CH1, AINPUTS_SINGLE_READ, &SendAnalogValue};/**< Conversor AD con entrada en CH1.*/
timer_config my_timer={TIMER_A, 1, &ReadAnalogValue};/**< Timer que dispara la lectura de valor ingresado en CH1.*/

/*==================[internal functions declaration]=========================*/

/** @fn ReadAnalogValue
 *
 * @brief Función que dispara la conversión AD.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ReadAnalogValue(void)
{
	AnalogStartConvertion();
}

/** @fn SendAnalogValue
 *
 * @brief Función que lee el valor ingresado en CH1. Y realiza los procesos generales del juego.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SendAnalogValue(void)
{
	if(finish){
		uint32_t segundos = game_time/1000;
		uint32_t milisegundos = game_time%1000;
		UartSendString(SERIAL_PORT_PC, info_static);
		UartSendString(SERIAL_PORT_PC, UartItoa(segundos, 10));
		UartSendString(SERIAL_PORT_PC, info_dot);
		UartSendString(SERIAL_PORT_PC, UartItoa(milisegundos, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");

		ILI9341Rotate(ILI9341_Landscape_2);
		ILI9341DrawString(90, 100, end_game, &font_16x26, ILI9341_GREENYELLOW, ILI9341_NAVY);
		start = false;
		finish = false;
		TimerStop(TIMER_A);
		TimerReset(TIMER_A);
	}

	if(start){
		if(refresh == (refresh_time+1) ){
			refresh = 0;
		}
		if(pos_samples == 5){
			pos_samples = 0;
		}
		AnalogInputRead(my_analog.input , &analog_value_input);
		analog_samples[pos_samples] = analog_value_input;
		pos_samples++;

		if(refresh == refresh_time){
			DrawPaddle();
			DrawBall();
			DrawCourt();
			if( pos_ball_y < 5){
					finish = true;
				}
		}
		game_time++;

		if( (game_time%CHANGE_REFRESH_SCREEN == 0) && refresh_time>10 ){
			refresh_time = (refresh_time-5);
		}
		if( (game_time%CHANGE_SPEED_BALL == 0) && refresh_time == 10 ){
			if(speed_ball < 10){
				speed_ball = speed_ball +2;
			}
		}

		refresh++;
	}

	rand_time++;
}

/** @fn CalcularPosPaddle
 *
 * @brief Función que calcula una nueva posición para la paleta.
 * @param[in] No recibe parámetros.
 *
 * @return Nueva posición de la paleta.
 */
uint16_t CalcularPosPaddle(void){
	uint16_t value = 0;
	for(uint8_t i = 0; i<5; i++){
		value = value + analog_samples[i];
	}
	value = value / SAMPLE_SIZE;
	value = ((value-28) * (220.0/995.0)) +10;
	return value;
}

/** @fn CalcularPosXBall
 *
 * @brief Función que calcula una nueva posición en el eje X para la pelota.
 * @param[in] No recibe parámetros.
 *
 * @return Nueva posición en eje X de la pelota.
 */
uint16_t CalcularPosXBall(void){
	uint16_t new_x=0;

	if( ((old_x + RADIO_BALL) >= (ILI9341_WIDTH - WALL_THICKNESS)) || ((old_x - RADIO_BALL) <= WALL_THICKNESS)  ){
		delta_x = (delta_x*-1);
	}
	if( (old_y + RADIO_BALL) >= (ILI9341_HEIGHT - WALL_THICKNESS) ){
		delta_x = game_time%5 + speed_ball;
		if(game_time%2){
			delta_x = (delta_x*-1);
		}
	}
	if( old_y < ILI9341_HEIGHT/3 && (old_x - RADIO_BALL) <= WALL_THICKNESS ){
		delta_x = (game_time%5 + speed_ball);
	}
	if( old_y < ILI9341_HEIGHT/3 && (old_x + RADIO_BALL) >= (ILI9341_WIDTH - WALL_THICKNESS) ){
		delta_x = (game_time%5 + speed_ball)*-1 ;
	}

	new_x = old_x + delta_x;

	return new_x;
}

/** @fn CalcularPosYBall
 *
 * @brief Función que calcula una nueva posición en el eje Y para la pelota.
 * @param[in] No recibe parámetros.
 *
 * @return Nueva posición en eje Y de la pelota.
 */
uint16_t CalcularPosYBall(void){
	uint16_t new_y=0;
	if( (old_y + RADIO_BALL) >= (ILI9341_HEIGHT - WALL_THICKNESS)  ){
		delta_y = (rand_time%5 + speed_ball)*-1 ;
		rand_time = rand_time>>1;
		if(delta_y == 0){
			delta_y = -5;
		}
	}
	if( old_y < ILI9341_HEIGHT/3 && ( (old_x - RADIO_BALL) <= WALL_THICKNESS || (old_x + RADIO_BALL) >= (ILI9341_WIDTH - WALL_THICKNESS) ) ){
		delta_y = (rand_time%5 + speed_ball)*-1 ;
	}


	if( ((old_y - RADIO_BALL) <= 10) && ((old_y - RADIO_BALL) >= 5) ){
		if(( ((old_x) <= (pos_paddle + RADIO_PADDLE)) && ((old_x) >= (pos_paddle - RADIO_PADDLE)) )){
			delta_y = (delta_y*-1);
			if(delta_x == 0){
				delta_x = (game_time%5 + speed_ball)+1;
				if(game_time%2){
						delta_x = (delta_x*-1);
					}
			}
		}
	}

	if(begin){
//		delta_y = delta_y*-1;
		begin = false;
	}

	new_y = old_y + delta_y;

	return new_y;
}

/** @fn DrawPaddle
 *
 * @brief Función que dibuja la paleta en la pantalla led.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DrawPaddle(){
	ILI9341DrawFilledRectangle( (pos_paddle - RADIO_PADDLE), 1 , (pos_paddle + RADIO_PADDLE) , 9 , ILI9341_DARKCYAN);

	pos_paddle = CalcularPosPaddle();

	ILI9341DrawFilledRectangle( (pos_paddle - RADIO_PADDLE), 1 , (pos_paddle + RADIO_PADDLE) , 9 , ILI9341_RED);
}

/** @fn DrawBall
 *
 * @brief Función que dibuja la pelota en la pantalla led.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DrawBall(){
	ILI9341DrawFilledCircle( pos_ball_x, pos_ball_y , RADIO_BALL , ILI9341_DARKCYAN);
	old_x = pos_ball_x;
	old_y = pos_ball_y;
	pos_ball_x = CalcularPosXBall();
	pos_ball_y = CalcularPosYBall();
	ILI9341DrawFilledCircle( pos_ball_x, pos_ball_y , RADIO_BALL , ILI9341_GREENYELLOW);
}

/** @fn DrawCourt
 *
 * @brief Función que dibuja las paredes de  la cancha en la pantalla led.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DrawCourt(){
	if(pos_ball_y > (ILI9341_HEIGHT/2 -10) ){
		ILI9341DrawFilledRectangle( 0 , (ILI9341_HEIGHT - WALL_THICKNESS) , ILI9341_WIDTH , ILI9341_HEIGHT , ILI9341_NAVY);
		ILI9341DrawFilledRectangle( (ILI9341_WIDTH - WALL_THICKNESS), (ILI9341_HEIGHT/3) , ILI9341_WIDTH , ILI9341_HEIGHT , ILI9341_NAVY);
		ILI9341DrawFilledRectangle( 0 , (ILI9341_HEIGHT/3) ,  5 , ILI9341_HEIGHT , ILI9341_NAVY);

	} else{
		ILI9341DrawFilledRectangle( (ILI9341_WIDTH - WALL_THICKNESS), 10 , ILI9341_WIDTH , (ILI9341_HEIGHT/3) , ILI9341_DARKGREY);
		ILI9341DrawFilledRectangle( 0 , 10 ,  WALL_THICKNESS , (ILI9341_HEIGHT/3) , ILI9341_DARKGREY);
	}
}

/** @fn DrawInit
 *
 * @brief Función que dibuja el fondo y paredes en la pantalla led.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DrawInit(){

	ILI9341Fill(ILI9341_DARKCYAN);

	ILI9341DrawFilledRectangle( 0 , (ILI9341_HEIGHT - WALL_THICKNESS) , ILI9341_WIDTH , ILI9341_HEIGHT , ILI9341_NAVY);
	ILI9341DrawFilledRectangle( (ILI9341_WIDTH - WALL_THICKNESS), (ILI9341_HEIGHT/3) , ILI9341_WIDTH , ILI9341_HEIGHT , ILI9341_NAVY);
	ILI9341DrawFilledRectangle( 0 , (ILI9341_HEIGHT/3) ,  WALL_THICKNESS , ILI9341_HEIGHT , ILI9341_NAVY);

	ILI9341DrawFilledRectangle( (ILI9341_WIDTH - WALL_THICKNESS), 10 , ILI9341_WIDTH , (ILI9341_HEIGHT/3) , ILI9341_DARKGREY);
	ILI9341DrawFilledRectangle( 0 , 10 ,  WALL_THICKNESS , (ILI9341_HEIGHT/3) , ILI9341_DARKGREY);
}

/** @fn SysInit
 *
 * @brief Función que inicia todos los drivers a utilizarse en la aplicación.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SysInit(void)
{
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = NULL;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	AnalogOutputInit();
	AnalogInputInit(&my_analog);
	UartInit(&UART_USB);

	ILI9341Init(SPI_1, GPIO_5, GPIO_1, GPIO_3);
	ILI9341Rotate(ILI9341_Portrait_2);

	DrawInit();

	TimerStart(TIMER_A);
}

/** @fn StartGame
 *
 * @brief Función que inicia el juego.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void StartGame(void){
	start = !start;
	pos_ball_x = (rand_time%220)+10 ;
	delta_x = (rand_time%5)+1;
	delta_y = (rand_time%5)+1;
	UartSendString(SERIAL_PORT_PC, info_start);
}

/** @fn ResetGame
 *
 * @brief Función que reinicia parámetros para un nuevo inicio de juego.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ResetGame(void){
	start = false;
	begin = true;
	finish = false;
	rand_time = 0;
	game_time = 0;

	pos_paddle = 10;
	pos_ball_x = 0;
	pos_ball_y = 13;

	refresh = 0;
	refresh_time = 100;
	speed_ball = 0;

	ILI9341Rotate(ILI9341_Portrait_2);

	DrawInit();

	TimerStart(TIMER_A);
}


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	SwitchActivInt(SWITCH_1, StartGame);
	SwitchActivInt(SWITCH_2, ResetGame);
	while(1){

	}

    return 0;
}

/*==================[end of file]============================================*/

