/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "aplicacion_proyecto_dos.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "tcrt5000.h"

/*==================[macros and definitions]=================================*/

#define LIMITE 10
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

void LedOnValorBCD(uint8_t digito){

	uint8_t MASK = 1;

	//Bit 0 del digito
	if(digito&MASK){
		LedOn(LED_3);
		MASK = MASK << 1;
	} else{
		LedOff(LED_3);
		MASK = MASK << 1;
	  }
	//Bit 2 del digito
	if(digito&MASK){
		LedOn(LED_2);
		MASK = MASK << 1;
	} else{
		LedOff(LED_2);
		MASK = MASK << 1;
	  }
	//Bit 3 del digito
	if(digito&MASK){
		LedOn(LED_1);
		MASK = MASK << 1;
	} else{
		LedOff(LED_1);
		MASK = MASK << 1;
	  }
	//Bit 4 del digito
		if(digito&MASK){
			LedOn(LED_RGB_B);
			MASK = MASK << 1;
		} else{
			LedOff(LED_RGB_B);
			MASK = MASK << 1;
		  }

}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LedOn(LED_3);
	DelaySec(2);
	LedOff(LED_3);


	Tcrt5000Init(GPIO_T_COL0);
	uint8_t contador= 0;

	while(1){
		if( Tcrt5000Cuenta() ){
			contador++;
			if(contador == LIMITE){
				contador= 0;
			}
			LedOnValorBCD(contador);

		}
	}


    
    return 0;
}

/*==================[end of file]============================================*/

