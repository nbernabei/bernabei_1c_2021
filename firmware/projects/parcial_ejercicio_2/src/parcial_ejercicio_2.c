/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Parcial Ejercicio 2
 *
 * @section genDesc General Description
 *
 * Esta aplicación implementa un sistema que realiza un control de fase digital para controlar la velocidad de un motor universal.
 * Se necesita digitaliza la senoidal de la red con 1 ms de resolución y detectar el cruce por cero, y luego de cierto tiempo,
 * dispara un triac con un pulso de 1ms.
 *
 * @section hardConn Hardware Connection
 *
 *
 * |   EXTERN 		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | ANALOG SIGNAL	| 	CAD - CH1	|
 * | 	TRIAC	 	| 	T_COL0		|
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Creación del documento.		                 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/parcial_ejercicio_2.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/

#define MASK_S 0x53
/**
 * @def MASK_S
 * @brief Máscara de comparación para entrada por teclado.
 */

#define MASK_B 0x42
/**
 * @def MASK_B
 * @brief Máscara de comparación para entrada por teclado.
 */

#define BIT_CAD 9
/**
 * @def BIT_CAD
 * @brief Bit más significativo del conversor AD.
 */


#define MASK_COMPARATION 1<<BIT_CAD
/**
 * @def MASK_COMPARATION
 * @brief Máscara de comparación para el valor medio del conversor.
 */


#define MIN_R0 0
/**
 * @def MIN_R0
 * @brief Valor mínimo que puede tomar R0.
 */


#define MAX_R0 20
/**
 * @def MAX_R0
 * @brief Valor máximo que puede tomar R0.
 */


/*==================[internal data definition]===============================*/

uint8_t R0 = 0; /**< Tiempo de espera antes de disparar el triac.*/
uint16_t current_input = 0; /**< Valor de señal analógica digitalizada actual.*/
uint16_t last_input = 0; /**< Valor de señal analógica digitalizada anterior.*/
gpio_t output_port = GPIO_T_COL0; /**< Puerto de conexión al triac.*/

void UpR0(void);
void DownR0(void);
void ReadKeyboard(void);
void ReadAnalogValue(void);
void EvaluateCrossZero(void);
void ShootTimer(void);
void TriggerTriac(void);

analog_input_config my_analog={CH1, AINPUTS_SINGLE_READ, &EvaluateCrossZero};/**< Conversor AD con entrada en CH1.*/
timer_config my_timer = {TIMER_A, 1, &ReadAnalogValue};/**< Timer que dispara la función de medición.*/
timer_config my_timer_b = {TIMER_B, R0, &TriggerTriac};/**< Timer que dispara la espera a la activacion del triac.*/

/*==================[internal functions declaration]=========================*/

/** @fn ReadAnalogValue
 *
 * @brief Función que dispara la conversión AD.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ReadAnalogValue(void)
{
	AnalogStartConvertion();
}

/** @fn EvaluateCrossZero
 *
 * @brief Función que evalua un cruce por cero de la señal analógica y controla el disparo del triac.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void EvaluateCrossZero(void)
{
	if(trigger){
		trigger = false;
		GPIOOff(output_port);
	}

	AnalogInputRead(my_analog.input , &current_input);

	if( (current_input < MASK_COMPARATION) && (last_input > MASK_COMPARATION) ){
		ShootTimer();
	} else if( (current_input > MASK_COMPARATION) && (last_input < MASK_COMPARATION) ){
		ShootTimer();
	}

	last_input = current_input;
	if(trigger){
		GPIOOn(output_port);
	}

}

/** @fn ShootTimer
 *
 * @brief Función que inicia el timer B.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ShootTimer(void){
	TimerStart(TIMER_B);
}

/** @fn TriggerTriac
 *
 * @brief Función que pone en 1 a la variable trigger.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void TriggerTriac(void){
	trigger = true;
}

/** @fn UpR0
 *
 * @brief Función que aumenta el valor de R0.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void UpR0(void){
	if(R0 < MAX_R0){
		R0 = R0 +5;
	}
}

/** @fn DownR0
 *
 * @brief Función que disminuye el valor de R0.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DownR0(void){
	if(R0 > MIN_R0){
		R0 = R0 -5;
	}
}

/** @fn ReadKeyboard
 *
 * @brief Función que toma información de la PC y evalúa si es un comando válido.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ReadKeyboard(void){
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);

	if(dat==MASK_B){
		DownR0();
	}
	if(dat==MASK_S){
		UpR0();
	}
}

/** @fn SysInit
 *
 * @brief Función que inicia todos los drivers a utilizarse en la aplicación.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SysInit(void)
{
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = ReadKeyboard;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	UartInit(&UART_USB);
	AnalogOutputInit();
	AnalogInputInit(&my_analog);
	GPIOInit(output_port, GPIO_OUTPUT);

	TimerStart(TIMER_A);
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	while(1){

	}

    return 0;
}

/*==================[end of file]============================================*/

