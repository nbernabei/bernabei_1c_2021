/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Parcial Ejercicio 1
 *
 * @section genDesc General Description
 *
 * Esta aplicación implementa un sistema de control de velocidad de vehículos en el ingreso de una cochera utilizando el HC-SR04.
 * Se mide la velocidad del vehículo al sensor HC-SR04
 *
 * @section hardConn Hardware Connection
 *
 *
 * |   HC-SR04		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	T_FIL2		|
 * | 	PIN2	 	| 	T_FIL3		|
 *
 * |     PC   		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	USB	 	 	| 	PORT_PC		|
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Creación del documento.		                 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/parcial_ejercicio_1.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "hc_sr4.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/

#define DELTA_TIME 100
/**
 * @def DELTA_TIME 100
 * @brief Constante de tiempo para calcular la velocidad del vehículo.
 */

#define CONSTANT_RELATION 1000
/**
 * @def CONSTANT_RELATION 1000
 * @brief Constante para que exista una correcta relación entre la distancia y el tiempo para calcular la velocidad.
 */
#define FIRST_PARAMETER 3
/**
 * @def FIRST_PARAMETER 3
 * @brief Parámetro para determinar la configuración de LEDs.
 */
#define SECOND_PARAMETER 8
/**
 * @def SECOND_PARAMETER 8
 * @brief Parámetro para determinar la configuración de LEDs.
 */

/*==================[internal data definition]===============================*/

bool power;/**< Bandera que determina el encendido y apagado del sistema de control.*/
uint16_t current_measure = 0; /**< Valor de distancia actual al vehículo.*/
uint16_t last_measure = 0; /**< Valor de distancia anterior al vehículo.*/
uint8_t car_speed = 0; /**< Valor de velocidad del vehículo.*/
char info_static []={" m/s \r"};/**< String que complementa a la información que se envía a la PC.*/

void Measurement(void);
void ConfigureLowSpeed(void);
void ConfigureMediumSpeed(void);
void ConfigureHighSpeed(void);

timer_config my_timer={TIMER_A, 100, &Measurement};/**< Timer que dispara la función de medición.*/

/*==================[internal functions declaration]=========================*/

/** @fn Measurement
 *
 * @brief Función que detecta la velocidad del vehículo hacia el sensor de ultrasonido.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void Measurement(void)
{
	if(power){
		current_measure = HcSr04ReadDistanceCentimeters();
		current_measure = current_measure * CONSTANT_RELATION;
		car_speed = (current_measure - last_measure) / DELTA_TIME;

		if(car_speed < FIRST_PARAMETER){
			ConfigureLowSpeed();
		} else if(car_speed < SECOND_PARAMETER){
			ConfigureMediumSpeed();
		} else{
			ConfigureHighSpeed();
		}

		UartSendString(SERIAL_PORT_PC, UartItoa(car_speed, 10));
		UartSendString(SERIAL_PORT_PC, info_static);

		last_measure = current_measure;
	}
}

/** @fn ConfigureLowSpeed
 *
 * @brief Función que configura los LEDs para velocidad baja.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ConfigureLowSpeed(void){
	LedOff(LED_1);
	LedOff(LED_2);
	LedOn(LED_3);
}

/** @fn ConfigureMediumSpeed
 *
 * @brief Función que configura los LEDs para velocidad media.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ConfigureMediumSpeed(void){
	LedOff(LED_1);
	LedOff(LED_3);
	LedOn(LED_2);
}

/** @fn ConfigureHighSpeed
 *
 * @brief Función que configura los LEDs para velocidad alta.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ConfigureHighSpeed(void){
	LedOff(LED_3);
	LedOff(LED_2);
	LedOn(LED_1);
}

/** @fn SysInit
 *
 * @brief Función que inicia todos los drivers a utilizarse en la aplicación.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SysInit(void)
{
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = NULL;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	UartInit(&UART_USB);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	TimerStart(TIMER_A);

	power=false;
}

/** @fn SwitchPower
 *
 * @brief Función que enciende y apaga el sistema de control.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SwitchPower(void){
	power = !power;
	if(power){
		LedOn(LED_RGB_B);
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	SwitchActivInt(SWITCH_1, SwitchPower);

	while(1){

	}

    return 0;
}

/*==================[end of file]============================================*/

