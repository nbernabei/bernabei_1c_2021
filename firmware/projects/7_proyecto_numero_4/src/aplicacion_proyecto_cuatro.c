/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Bernabei
 *
 *
 *
 * Revisión:
 * 19-04-21: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @mainpage Osciloscopio Digital
 *
 * @section genDesc General Description
 *
 * Esta aplicación permite visualizar en una aplicación de osciloscopio digital en PC, una señal analógica que ingresa
 * a la EDU-CIAA. La señal que ingresa al conversor A/D, es un señal ECG digitalizada que sale por el conversor DA de la misma EDU-CIAA.
 * Se utiliza el software "Serial Oscilloscope" recomendado por la cátedra.
 *
 * @section hardConn Hardware Connection
 *
 *
 * |   EDU-CIAA		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DAC (P1)	| 	CH1(P1)		|
 *
 * |     PC   		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	USB	 	 	| 	PORT_PC		|
 *
 * @section changelog Changelog
 *
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 28/05/2021 | Creación del documento.		                 |
 *
 * @author Nicolas Bernabei
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/aplicacion_proyecto_cuatro.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "tcrt5000.h"
#include "DisplayITS_E0803.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "analog_io.h"


/*==================[macros and definitions]=================================*/

#define BUFFER_SIZE 231
/**
 * @def BUFFER_SIZE 231
 * @brief Tamaño del vector ECG que se visualiza.
 */

/*==================[internal data definition]===============================*/

bool write_analog; /**< Variable para que la frecuencia de escritura sea la mitad que la de lectura.*/
bool filtering; /**< Variable para activar y desactivar el filtrado.*/
uint16_t analog_value_input; /**< Valor que ingresa a CH1 del conversor AD.*/
uint16_t filtered_output = 0; /**< Valor se envía al osciloscopio digital. Se inicializa en cero.*/
uint16_t last_filtered_output = 0; /**< Último valor enviado al osciloscopio digital. Se inicializa en cero.*/
char info_static []={" \r"};/**< String que complementa a la información que se envía a la PC.*/

float alpha= 1.0;/**< Factor que se utiliza para calcular el filtrado de la señal.Se inicializa en 1, varía de 0 a 1.*/
const float pi= 3.14159;/**< Constante con un valor aproximado de PI. Se utiliza en el cálculo de alpha.*/
uint8_t frequency_cut= 60; /**< Valor de la frecuencia de corte. Se utiliza en el cálculo de alpha. Se inicializa en 60 Hz. Varía de a 5 Hz.*/

uint8_t pos_ecg = 0; /**< Posición que recorre el arreglo de ECG.*/
const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};/**< Arreglo de ECG */

void ReadAnalogValue(void);
void SendAnalogValue(void);

analog_input_config my_analog={CH1, AINPUTS_SINGLE_READ, &SendAnalogValue};/**< Conversor AD con entrada en CH1.*/
timer_config my_timer={TIMER_A, 1000, &ReadAnalogValue};/**< Timer que dispara la lectura de valor ingresado en CH1.*/

/*==================[internal functions declaration]=========================*/

/** @fn ReadAnalogValue
 *
 * @brief Función que dispara la conversión AD.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void ReadAnalogValue(void)
{
	AnalogStartConvertion();
}

/** @fn SendAnalogValue
 *
 * @brief Función que lee el valor ingresado en CH1, y lo envía a la PC. El valor puede ser filtrado o no. Además controla la frecuencia de escritura del conversor DA.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SendAnalogValue(void)
{
	if(write_analog){
		AnalogOutputWrite(ecg[pos_ecg]);
		pos_ecg++;
		if(pos_ecg == BUFFER_SIZE){
			pos_ecg = 0;
		}
	}
	write_analog = !write_analog;

	AnalogInputRead(my_analog.input , &analog_value_input);

	if(filtering){
		filtered_output = last_filtered_output+ alpha*(analog_value_input - last_filtered_output);
	} else{
		filtered_output = analog_value_input;
	}


	UartSendString(SERIAL_PORT_PC, UartItoa(filtered_output, 10));
	UartSendString(SERIAL_PORT_PC, info_static);

	last_filtered_output = filtered_output;
}



/** @fn SysInit
 *
 * @brief Función que inicia todos los drivers a utilizarse en la aplicación.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void SysInit(void)
{
	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = ReadAnalogValue;

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	TimerInit(&my_timer);
	AnalogOutputInit();
	AnalogInputInit(&my_analog);
	UartInit(&UART_USB);

	TimerStart(TIMER_A);

	write_analog=true;
	filtering=false;
}

/** @fn FilterOn
 *
 * @brief Función que activa el filtrado de la señal enviada a la PC.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void FilterOn(void)
{
	if(!filtering){
		filtering = 1;
	}
}

/** @fn FilterOff
 *
 * @brief Función que desactiva el filtrado de la señal enviada a la PC.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void FilterOff(void)
{
	if(filtering){
		filtering = 0;
	}
}

/** @fn DownFrequency
 *
 * @brief Función que disminuye la frecuencia de corte.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void DownFrequency(void)
{
	if(frequency_cut > 0){
		frequency_cut = frequency_cut -10;
		if(frequency_cut == 0){
			frequency_cut = 1;
		}
		alpha= 0.002/((1/(2.0*pi*frequency_cut))+0.002);
	}
}

/** @fn UpFrequency
 *
 * @brief Función que aumenta la frecuencia de corte.
 * @param[in] No recibe parámetros.
 *
 * @return No retorna ningún valor.
 */
void UpFrequency(void)
{
	if(frequency_cut < 80){
		frequency_cut = frequency_cut +5;
		alpha= 0.002/((1/(2.0*pi*frequency_cut))+0.002);
	}
}
/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void)
{
	SysInit();

	SwitchActivInt(SWITCH_1, FilterOn);
	SwitchActivInt(SWITCH_2, FilterOff);
	SwitchActivInt(SWITCH_3, DownFrequency);
	SwitchActivInt(SWITCH_4, UpFrequency);

	while(1){

	}

    return 0;
}

/*==================[end of file]============================================*/

