

/*==================[inclusions]=============================================*/
#include "../inc/DisplayITS_E0803.h"       /* <= own header */
#include "gpio.h"
#include "led.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/



/*==================[internal data definition]===============================*/

/*==================[external variables]==============================*/
/**
 * @def MASC
 * @brief Máscara de comparación para funciones utilizadas por el dispositivo ITSE0803.
 */
#define MASC 0x01
gpio_t BCD_gpio[4];	/**< Arreglo de puertos utilizados por las funciones del dispositivo ITSE0803*/
gpio_t OrdenSeleccion[3];	/**< Arreglo para ordenar los dígitos del número a mostrar en el dispositivo ITSE0803*/

uint8_t NumElegido;

/*==================[internal functions declaration]=========================*/

/** @fn ConversorUnDecCen
 *
 * @brief Convierte un número de tres cifras en un arreglo de 3 numeros separados.
 *
 * @param[in] data: valor del número, digits: cantidad de dígitos del número
 *
 * @return No retorna ningún parámetro
 */
void  ConversorUnDecCen (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t aux;
	uint8_t i;

	for ( i = 0; i < digits; i++){
		aux = data % 10;
		bcd_number[digits-i-1] = aux;
		data -= aux;
		data = data/10;
	}}




/** @fn graficarBCD
 *
 * @brief Muestra en pantalla LCD un dígito.
 *
 * @param[in] BCD: Dígito que muestra
 *
 * @return No retorna ningún parámetro
 */
void graficarBCD( uint8_t BCD){
	uint8_t bcd[4];
	uint8_t i;
	for (i = 0; i < 4; i++){
	bcd[i] = (BCD >> i )& MASC;
	}

		if (bcd[0] == 0){
			GPIOOff(BCD_gpio[0]);}
		else{
			GPIOOn(BCD_gpio[0]);}


		if (bcd[1] == 0){
			GPIOOff(BCD_gpio[1]);}
		else{
			GPIOOn(BCD_gpio[1]);}


		if (bcd[2] == 0){
			GPIOOff(BCD_gpio[2]);}
		else{
			GPIOOn(BCD_gpio[2]);}


		if (bcd[3] == 0){
			GPIOOff(BCD_gpio[3]);}
		else{
			GPIOOn(BCD_gpio[3]);}
	}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

/** @fn ITSE0803Init
 *
 * @brief Funcion que inicializa el dispositivo pantalla LCD.
 *
 * @param[in] pins: puertos que inicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool ITSE0803Init(gpio_t * pins){


	BCD_gpio[0] = pins[0];
	BCD_gpio[1] = pins[1];
	BCD_gpio[2] = pins[2];
	BCD_gpio[3] = pins[3];

	OrdenSeleccion[0] = pins[4];
	OrdenSeleccion[1] = pins[5];
	OrdenSeleccion[2] = pins[6];

	GPIOInit(pins[0], GPIO_OUTPUT);
	GPIOInit(pins[1], GPIO_OUTPUT);
	GPIOInit(pins[2], GPIO_OUTPUT);
	GPIOInit(pins[3], GPIO_OUTPUT);
	GPIOInit(pins[4], GPIO_OUTPUT);
	GPIOInit(pins[5], GPIO_OUTPUT);
	GPIOInit(pins[6], GPIO_OUTPUT);



	return 1;
}

/** @fn ITSE0803DisplayValue
 *
 * @brief Funcion que muestra un valor a través del dispositivo pantalla LCD.
 *
 * @param[in] valor: número que mostrar en pantalla
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool ITSE0803DisplayValue(uint16_t valor){
	uint8_t dcu[3];  /*decena centena unidad*/
	NumElegido = valor;
	//if(0 <= valor <= 999){

		ConversorUnDecCen(valor, 3, dcu);

		graficarBCD(dcu[0]);
		GPIOOn(OrdenSeleccion[0]);
		GPIOOff(OrdenSeleccion[0]);


		graficarBCD(dcu[1]);
		GPIOOn(OrdenSeleccion[1]);
		GPIOOff(OrdenSeleccion[1]);


		graficarBCD(dcu[2]);
		GPIOOn(OrdenSeleccion[2]);
		GPIOOff(OrdenSeleccion[2]);


//		return true;
//	}
//	else
//		return false;
}

uint16_t ITSE0803ReadValue(void){
	return NumElegido;
}

/** @fn ITSE0803DeInit
 *
 * @brief Funcion no implementada.
 *
 * @param[in] pins: puertos que desinicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool ITSE0803Deinit(gpio_t * pins){
return 1;
}


/*==================[end of file]============================================*/

