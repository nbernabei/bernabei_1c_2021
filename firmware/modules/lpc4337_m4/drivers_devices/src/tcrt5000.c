
/*==================[inclusions]=============================================*/
#include "tcrt5000.h"       /* <= own header */

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
gpio_t puerto; /**< Variable global para asignar el puerto de conexión*/

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
/** @fn Tcrt5000Init
 *
 * @brief Funcion que inicializa el dispositivo sensor infrarrojo TCRT5000.
 *
 * @param[in] dout: puerto que inicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */

bool Tcrt5000Init(gpio_t dout)
{
	puerto = dout;
	GPIOInit(puerto, GPIO_INPUT);
	return true;
}

/** @fn Tcrt5000State
 *
 * @brief Funcion que informa el estado de salida del dispositivo TCRT5000.
 *
 * @param no recibe parámetros
 *
 * @return FALSE si se detecta un obstáculo, en otro caso retorna TRUE
 */

bool Tcrt5000State(void)
{
	return GPIORead(puerto);
}

/** @fn Tcrt5000Lectura
 *
 * @brief Función que sirve para controlar una correcta conexión del dispositivo TCRT5000.
 * En caso de una correcta conexión se enciende el LED 2 de la EDU-CIAA.
 *
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
void Tcrt5000Lectura(void)
{
	if( Tcrt5000State() == 0){
		LedOff(LED_2);
	} else{
		LedOn(LED_2);
	}
}


/** @fn Tcrt5000Cuenta
 *
 * @brief Registra el paso de un objeto, sucediendo un evento de flanco descendente
 * y otro evento de flanco ascendente se ratifica el conteo .
 *
 * @param No recibe parámetros
 *
 * @return FALSE si no ocurre un conteo, en caso de que se produzca el conteo retorna TRUE
 */
bool Tcrt5000Cuenta(void)
{
	bool cuenta= 0;
	static bool control= 0;

	if(Tcrt5000State() ==0 && control==0){
		control= 1;
	}
	if(Tcrt5000State() ==1 && control == 1){
		control= 0;
		cuenta= 1;
	}

	return cuenta;
}


/** @fn Tcrt5000DeInit
 *
 * @brief Funcion que desinicializa el dispositivo TCRT5000. No se encuentra implementada.
 *
 * @param[in] dout: puerto que desinicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool Tcrt5000Deinit(gpio_t dout)
{
	//GPIODeInit(void);
	return true;
}

/*==================[end of file]============================================*/

