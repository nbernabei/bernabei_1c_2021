
/** @mainpage hc_sr4.h
 *
 * \section genDesc Descripción General
 *
 * Este driver se encarga de recibir y procesar la informacion del periferico
 * de medidor de distancia por ultrasonido con el fin de obtener un dato numerico
 * el cual representa la distancia del sensor al objeto.
 * Tiene como rango de medición de 2 a 400 cm.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO2		|
 * | 	PIN2	 	| 	GPIO3		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/05/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Vera Marco
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H
/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HC_SR4
 ** @{
 * @file hc_sr4.h


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "bool.h"
#include "gpio.h"
/*==================[external functions declaration]=========================*/


/** @brief Initialization function of EDU-CIAA
 * Inicializa los pines en la EDU-CIAA
 * @param[in] gpio de los puertos de entrada y salida
 * @return TRUE si no hay errores
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @brief Function de lectura de la distancia en centimetros
 *
 *	Interpreta el ancho de pulso del sensor y lo transforma una medida en centimetros
 * @return un valor de distancia en centimetro
 */
uint16_t HcSr04ReadDistanceCentimeters(void);

/** @brief Function de lectura de la distancia en pulgadas
 *
 *	Interpreta el ancho de pulso del sensor y lo transforma una medida en pulgadas
 * @return un valor de distancia en pulgadas
 */
uint16_t HcSr04ReadDistanceInches(void);

/** @brief DeInitialization function of EDU-CIAA hc_sr4
 *
 * @return TRUE if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);



/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

