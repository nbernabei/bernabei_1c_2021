

#ifndef _TEMPLATE_H
#define _TEMPLATE_H
#include "gpio.h"
#include "bool.h"


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/


bool ITSE0803Init(gpio_t * pins);

bool ITSE0803DisplayValue(uint16_t valor);

uint16_t ITSE0803ReadValue(void);

bool ITSE0803Deinit(gpio_t * pins);


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

