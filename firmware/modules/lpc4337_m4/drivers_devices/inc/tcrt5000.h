/*! @mainpage Device TRCRT5000
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   TRCT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND	    	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/04/2021 | Creación del documento		                 |
 * | 16/04/2021	| Se añaden 3 funciones		                     |
 * | 			| 	                     						 |
 *
 * @author Nicolas Bernabei
 *
 */

#ifndef _TCRT5000_H
#define _TCRT5000_H

/*==================[macros]=================================================*/


/*==================[inclusions]=============================================*/

#include "bool.h"
#include "gpio.h"
#include "led.h"
#include <stdint.h>

/*==================[external functions declaration]=========================*/


bool Tcrt5000Init(gpio_t dout);

bool Tcrt5000State(void);

void Tcrt5000Lectura(void);

uint8_t Tcrt5000Contador(void);

bool Tcrt5000Deinit(gpio_t dout);

/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

