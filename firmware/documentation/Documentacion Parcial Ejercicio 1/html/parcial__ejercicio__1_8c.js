var parcial__ejercicio__1_8c =
[
    [ "CONSTANT_RELATION", "parcial__ejercicio__1_8c.html#a0fbed158168ffc5a1cfea74ff93c9d41", null ],
    [ "DELTA_TIME", "parcial__ejercicio__1_8c.html#aa3f852e6c642f1daf44080c2a2cd2799", null ],
    [ "FIRST_PARAMETER", "parcial__ejercicio__1_8c.html#acffb188c3a68141ed77cd84c3eec2a95", null ],
    [ "SECOND_PARAMETER", "parcial__ejercicio__1_8c.html#ac5090cf714b27434073788ef475a8115", null ],
    [ "ConfigureHighSpeed", "parcial__ejercicio__1_8c.html#afd343d1c4688a3374c69d624a8366d6d", null ],
    [ "ConfigureLowSpeed", "parcial__ejercicio__1_8c.html#a631fd1ef36bdb5c44b3621b14ff7c96b", null ],
    [ "ConfigureMediumSpeed", "parcial__ejercicio__1_8c.html#ae226723f10b6027a1d61234282d92664", null ],
    [ "main", "parcial__ejercicio__1_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "Measurement", "parcial__ejercicio__1_8c.html#a780dafdc7b51503401c719b9baedcc68", null ],
    [ "SwitchPower", "parcial__ejercicio__1_8c.html#ac95726161a8d308d4e090d584e7a6e1a", null ],
    [ "SysInit", "parcial__ejercicio__1_8c.html#a599b876befeecfecbda08c7514bd6317", null ],
    [ "car_speed", "parcial__ejercicio__1_8c.html#a0c13a35f27aa8ba43281a635b83f2273", null ],
    [ "current_measure", "parcial__ejercicio__1_8c.html#add489bf1935b4c9e8df5e5471df9859e", null ],
    [ "info_static", "parcial__ejercicio__1_8c.html#ade80b223741a4d3d37fa268c70bac96f", null ],
    [ "last_measure", "parcial__ejercicio__1_8c.html#aeef2756b9b0fb705742a76f1fb0f02a6", null ],
    [ "my_timer", "parcial__ejercicio__1_8c.html#aeaf95171809a97992e2f7a291aceecb6", null ],
    [ "power", "parcial__ejercicio__1_8c.html#aababa9aef0d20ddcfce2d78f41ae1dd8", null ]
];