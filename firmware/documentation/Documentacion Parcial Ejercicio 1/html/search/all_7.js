var searchData=
[
  ['info_5fstatic',['info_static',['../parcial__ejercicio__1_8c.html#ade80b223741a4d3d37fa268c70bac96f',1,'parcial_ejercicio_1.c']]],
  ['itse0803deinit',['ITSE0803Deinit',['../_display_i_t_s___e0803_8c.html#a261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../_display_i_t_s___e0803_8h.html#a261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803displayvalue',['ITSE0803DisplayValue',['../_display_i_t_s___e0803_8c.html#a7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c'],['../_display_i_t_s___e0803_8h.html#a7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c']]],
  ['itse0803init',['ITSE0803Init',['../_display_i_t_s___e0803_8c.html#a5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../_display_i_t_s___e0803_8h.html#a5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803readvalue',['ITSE0803ReadValue',['../_display_i_t_s___e0803_8c.html#a3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c'],['../_display_i_t_s___e0803_8h.html#a3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c']]]
];
