########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMReta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
#17-01-18: Versión inicial 
#
#########################################################################
#
# All rights reserved.
#
# This file is part of Workspace.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from this
#    software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#################################################################################

####Proyecto 0_blinking
#PROJECT = projects/0_blinking
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

####Proyecto 1_blinking_switch
#PROJECT = projects/1_blinking_switch
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

####Proyecto 2_blinking_switch_modif
#PROJECT = projects/2_blinking_switch_modif
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

####Proyecto 3_actividad_puerto_remoto
#PROJECT = projects/3_actividad_puerto_remoto
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

####Proyecto 4_ejercicio_teoria
#PROJECT = projects/4_ejercicio_teoria
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp


####Proyecto 5_proyecto_numero_dos
#PROJECT = projects/5_proyecto_numero_2
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

###Proyecto 5_proyecto_numero_dos
#PROJECT = projects/5_proyecto_numero_2_LCD
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

###Proyecto 3_blinking_switch_timer
#PROJECT = projects/3_blinking_switch_timer
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

###Proyecto 6_proyecto_numero_3
#PROJECT = projects/6_proyecto_numero_3
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

###Proyecto 7_proyecto_numero_4
#PROJECT = projects/7_proyecto_numero_4
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp

###Proyecto 8_proyecto_numero_5
PROJECT = projects/8_proyecto_numero_5
TARGET = lpc4337_m4
BOARD = edu_ciaa_nxp

###Proyecto3_UartRS232aUSB
#PROJECT = projects/3_UartRS232aUSB
#TARGET = lpc4337_m4
#BOARD = edu_ciaa_nxp
#-------------------------------------------------------------------------------
