var searchData=
[
  ['sample_5fsize',['SAMPLE_SIZE',['../aplicacion__proyecto__cinco_8c.html#ae25e0da7cdb20c758a56dc6aece92ba7',1,'aplicacion_proyecto_cinco.c']]],
  ['sendanalogvalue',['SendAnalogValue',['../aplicacion__proyecto__cinco_8c.html#aa44db27480e2b23a8ad4793cfb000c48',1,'aplicacion_proyecto_cinco.c']]],
  ['setchipselect',['SetChipSelect',['../ili9341_8c.html#aeabe935f1652950b6c43f10fe834c7bd',1,'ili9341.c']]],
  ['setcursorposition',['SetCursorPosition',['../ili9341_8c.html#a3ced3a9e7189797ae4b2c5cc34cc1886',1,'ili9341.c']]],
  ['sleep_5fin',['SLEEP_IN',['../ili9341_8c.html#ae9a82b50c8748942c64e90f7f8629669',1,'ili9341.c']]],
  ['sleep_5fout',['SLEEP_OUT',['../ili9341_8c.html#a50d64363b3ecae17415811bf896e2930',1,'ili9341.c']]],
  ['speed_5fball',['speed_ball',['../aplicacion__proyecto__cinco_8c.html#a4125c18a0f9c1e37d5d1a97e5e6806b5',1,'aplicacion_proyecto_cinco.c']]],
  ['spi_5fbr',['SPI_BR',['../ili9341_8c.html#a5a96556b55f3bf5350e836859a37280d',1,'ili9341.c']]],
  ['spi_5fconf',['spi_conf',['../ili9341_8c.html#ab6bcd62e69e810fbf184ed63cb90b487',1,'ili9341.c']]],
  ['start',['start',['../aplicacion__proyecto__cinco_8c.html#ab376b87f96a574a793c03c53e75afec8',1,'aplicacion_proyecto_cinco.c']]],
  ['startgame',['StartGame',['../aplicacion__proyecto__cinco_8c.html#ae419198eb7bcb4f54ab9aa17ea20e61d',1,'aplicacion_proyecto_cinco.c']]],
  ['stop_5fon_5fmatch',['STOP_ON_MATCH',['../delay_8c.html#a5eb14fe55a127213fc8961f49c95265f',1,'delay.c']]],
  ['switch',['Switch',['../group___switch.html',1,'']]],
  ['switch_2ec',['switch.c',['../switch_8c.html',1,'']]],
  ['switch_2eh',['switch.h',['../switch_8h.html',1,'']]],
  ['switch1int',['Switch1Int',['../switch_8c.html#a2e74731a30702d3acc7c0b0f7f41395f',1,'switch.c']]],
  ['switch2int',['Switch2Int',['../switch_8c.html#a0ad21f1345dc6349ca5e99d081534d91',1,'switch.c']]],
  ['switch3int',['Switch3Int',['../switch_8c.html#aab31211525294a15a34329d7209b9f82',1,'switch.c']]],
  ['switch4int',['Switch4Int',['../switch_8c.html#a1f3c7d2e8c5c4d0523c32c21c9715670',1,'switch.c']]],
  ['switch_5f1',['SWITCH_1',['../group___switch.html#ggaa87203a5637fb4759a378b579aaebff6a24caa46485c5fec2211b3ebd8a1eee77',1,'switch.h']]],
  ['switch_5f2',['SWITCH_2',['../group___switch.html#ggaa87203a5637fb4759a378b579aaebff6a73fda67835d5d51aa9251fee781a7434',1,'switch.h']]],
  ['switch_5f3',['SWITCH_3',['../group___switch.html#ggaa87203a5637fb4759a378b579aaebff6a2f2ac7a4c81e7f2add955b62dd667d79',1,'switch.h']]],
  ['switch_5f4',['SWITCH_4',['../group___switch.html#ggaa87203a5637fb4759a378b579aaebff6a2c5b32bc76013e9243c772f00c33409b',1,'switch.h']]],
  ['switchactivint',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c']]],
  ['switches',['SWITCHES',['../group___switch.html#gaa87203a5637fb4759a378b579aaebff6',1,'switch.h']]],
  ['switchesactivgroupint',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c']]],
  ['switchesinit',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['sysinit',['SysInit',['../aplicacion__proyecto__cinco_8c.html#a599b876befeecfecbda08c7514bd6317',1,'aplicacion_proyecto_cinco.c']]]
];
