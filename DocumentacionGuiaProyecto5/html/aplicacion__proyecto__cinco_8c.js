var aplicacion__proyecto__cinco_8c =
[
    [ "CHANGE_REFRESH_SCREEN", "aplicacion__proyecto__cinco_8c.html#ab822a4e092ead5f709a267796b19ad30", null ],
    [ "CHANGE_SPEED_BALL", "aplicacion__proyecto__cinco_8c.html#a9b269598a322a099f6f95bd5a8323a66", null ],
    [ "RADIO_BALL", "aplicacion__proyecto__cinco_8c.html#a9d1fa79c96990f5749d2d82eb7e88934", null ],
    [ "RADIO_PADDLE", "aplicacion__proyecto__cinco_8c.html#af4591afe73900de300570446b6936ef9", null ],
    [ "SAMPLE_SIZE", "aplicacion__proyecto__cinco_8c.html#ae25e0da7cdb20c758a56dc6aece92ba7", null ],
    [ "WALL_THICKNESS", "aplicacion__proyecto__cinco_8c.html#abf328a79134cdd4e6751eaa9fb6e1b0b", null ],
    [ "CalcularPosPaddle", "aplicacion__proyecto__cinco_8c.html#a172246f12533e4f7acc961c9fe5c30cc", null ],
    [ "CalcularPosXBall", "aplicacion__proyecto__cinco_8c.html#a2034e75130077bc24243cea6ed0015f2", null ],
    [ "CalcularPosYBall", "aplicacion__proyecto__cinco_8c.html#a6a81533c59aaf3da0059e30cf949c271", null ],
    [ "DrawBall", "aplicacion__proyecto__cinco_8c.html#ad1d9d3fafa534c5af7a591950b3a38b5", null ],
    [ "DrawCourt", "aplicacion__proyecto__cinco_8c.html#a56452b7ea4a1988cea7360022441fa43", null ],
    [ "DrawInit", "aplicacion__proyecto__cinco_8c.html#a9615e7464d806b57d16c18ed0967ad9c", null ],
    [ "DrawPaddle", "aplicacion__proyecto__cinco_8c.html#ac2252da3365c19d4ee673e59136a5a0f", null ],
    [ "main", "aplicacion__proyecto__cinco_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "ReadAnalogValue", "aplicacion__proyecto__cinco_8c.html#a4d6e098bd31a13d6843b98e7df6170cb", null ],
    [ "ResetGame", "aplicacion__proyecto__cinco_8c.html#aaf61cc995895212572bebce8fde83f8c", null ],
    [ "SendAnalogValue", "aplicacion__proyecto__cinco_8c.html#aa44db27480e2b23a8ad4793cfb000c48", null ],
    [ "StartGame", "aplicacion__proyecto__cinco_8c.html#ae419198eb7bcb4f54ab9aa17ea20e61d", null ],
    [ "SysInit", "aplicacion__proyecto__cinco_8c.html#a599b876befeecfecbda08c7514bd6317", null ],
    [ "analog_samples", "aplicacion__proyecto__cinco_8c.html#aa571407206bfe4aa10cd88b2b2415611", null ],
    [ "analog_value_input", "aplicacion__proyecto__cinco_8c.html#a10bebb66a59ede984e18428404318aa4", null ],
    [ "begin", "aplicacion__proyecto__cinco_8c.html#a60c7721c91ff4244bc6f7f92f2eefdaf", null ],
    [ "delta_x", "aplicacion__proyecto__cinco_8c.html#a460e7f701b0322fc97d978685f57f4f7", null ],
    [ "delta_y", "aplicacion__proyecto__cinco_8c.html#a7942b31cba534c1ab75692c8e4bcf88f", null ],
    [ "end_game", "aplicacion__proyecto__cinco_8c.html#a75a08263cf9dba88d167653c63ddaadc", null ],
    [ "finish", "aplicacion__proyecto__cinco_8c.html#ad24ebffc3b7cd5e04988c49fdcb80293", null ],
    [ "game_time", "aplicacion__proyecto__cinco_8c.html#ad072304fde0b231d70d0b631a0e8e256", null ],
    [ "info_dot", "aplicacion__proyecto__cinco_8c.html#a9d130dfb414b4479270efb5d127b7d6e", null ],
    [ "info_start", "aplicacion__proyecto__cinco_8c.html#ada1b2681d2b88c3f56507a9d8ecd9e41", null ],
    [ "info_static", "aplicacion__proyecto__cinco_8c.html#ade80b223741a4d3d37fa268c70bac96f", null ],
    [ "my_analog", "aplicacion__proyecto__cinco_8c.html#a8afeb6c155235aee7077523711c2f413", null ],
    [ "my_timer", "aplicacion__proyecto__cinco_8c.html#aeaf95171809a97992e2f7a291aceecb6", null ],
    [ "old_x", "aplicacion__proyecto__cinco_8c.html#abf09fd8f05b62ddd312bed00d9a0f84c", null ],
    [ "old_y", "aplicacion__proyecto__cinco_8c.html#a46e8566cf3ab6e80bdec6bffb0644d79", null ],
    [ "pos_ball_x", "aplicacion__proyecto__cinco_8c.html#aa8aa288607e004575941d3bdb3b5e2ed", null ],
    [ "pos_ball_y", "aplicacion__proyecto__cinco_8c.html#a1f52b7cd2947a2e334dcebced9d4657c", null ],
    [ "pos_paddle", "aplicacion__proyecto__cinco_8c.html#aea1bbc70c067eced5ab199f42445b5bb", null ],
    [ "pos_samples", "aplicacion__proyecto__cinco_8c.html#ac6ee28399d0e9124fb780f9d59012950", null ],
    [ "rand_time", "aplicacion__proyecto__cinco_8c.html#ab510d7af070e0e33472c60279f54b71a", null ],
    [ "refresh", "aplicacion__proyecto__cinco_8c.html#a306ef16785a7ce4bc51d0c2d8919b467", null ],
    [ "refresh_time", "aplicacion__proyecto__cinco_8c.html#a7e9560b6783acc7c615e3593e741c308", null ],
    [ "speed_ball", "aplicacion__proyecto__cinco_8c.html#a4125c18a0f9c1e37d5d1a97e5e6806b5", null ],
    [ "start", "aplicacion__proyecto__cinco_8c.html#ab376b87f96a574a793c03c53e75afec8", null ]
];