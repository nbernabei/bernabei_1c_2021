/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Módulos",url:"modules.html"},
{text:"Estructuras de Datos",url:"annotated.html",children:[
{text:"Estructura de datos",url:"annotated.html"},
{text:"Índice de estructura de datos",url:"classes.html"},
{text:"Campos de datos",url:"functions.html",children:[
{text:"Todo",url:"functions.html"},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Archivos",url:"files.html",children:[
{text:"Lista de archivos",url:"files.html"},
{text:"Globales",url:"globals.html",children:[
{text:"Todo",url:"globals.html",children:[
{text:"_",url:"globals.html#index__"},
{text:"a",url:"globals_a.html#index_a"},
{text:"b",url:"globals_b.html#index_b"},
{text:"c",url:"globals_c.html#index_c"},
{text:"d",url:"globals_d.html#index_d"},
{text:"e",url:"globals_e.html#index_e"},
{text:"f",url:"globals_f.html#index_f"},
{text:"g",url:"globals_g.html#index_g"},
{text:"h",url:"globals_h.html#index_h"},
{text:"i",url:"globals_i.html#index_i"},
{text:"l",url:"globals_l.html#index_l"},
{text:"m",url:"globals_m.html#index_m"},
{text:"n",url:"globals_n.html#index_n"},
{text:"o",url:"globals_o.html#index_o"},
{text:"p",url:"globals_p.html#index_p"},
{text:"r",url:"globals_r.html#index_r"},
{text:"s",url:"globals_s.html#index_s"},
{text:"t",url:"globals_t.html#index_t"},
{text:"u",url:"globals_u.html#index_u"},
{text:"v",url:"globals_v.html#index_v"},
{text:"w",url:"globals_w.html#index_w"}]},
{text:"Funciones",url:"globals_func.html",children:[
{text:"c",url:"globals_func.html#index_c"},
{text:"d",url:"globals_func.html#index_d"},
{text:"f",url:"globals_func.html#index_f"},
{text:"g",url:"globals_func.html#index_g"},
{text:"h",url:"globals_func.html#index_h"},
{text:"i",url:"globals_func.html#index_i"},
{text:"l",url:"globals_func.html#index_l"},
{text:"m",url:"globals_func.html#index_m"},
{text:"r",url:"globals_func.html#index_r"},
{text:"s",url:"globals_func.html#index_s"},
{text:"t",url:"globals_func.html#index_t"},
{text:"w",url:"globals_func.html#index_w"}]},
{text:"Variables",url:"globals_vars.html",children:[
{text:"a",url:"globals_vars.html#index_a"},
{text:"b",url:"globals_vars.html#index_b"},
{text:"c",url:"globals_vars.html#index_c"},
{text:"d",url:"globals_vars.html#index_d"},
{text:"e",url:"globals_vars.html#index_e"},
{text:"f",url:"globals_vars.html#index_f"},
{text:"g",url:"globals_vars.html#index_g"},
{text:"i",url:"globals_vars.html#index_i"},
{text:"l",url:"globals_vars.html#index_l"},
{text:"m",url:"globals_vars.html#index_m"},
{text:"n",url:"globals_vars.html#index_n"},
{text:"o",url:"globals_vars.html#index_o"},
{text:"p",url:"globals_vars.html#index_p"},
{text:"r",url:"globals_vars.html#index_r"},
{text:"s",url:"globals_vars.html#index_s"},
{text:"v",url:"globals_vars.html#index_v"}]},
{text:"Enumeraciones",url:"globals_enum.html"},
{text:"Valores de enumeraciones",url:"globals_eval.html"},
{text:"defines",url:"globals_defs.html",children:[
{text:"_",url:"globals_defs.html#index__"},
{text:"b",url:"globals_defs.html#index_b"},
{text:"c",url:"globals_defs.html#index_c"},
{text:"d",url:"globals_defs.html#index_d"},
{text:"e",url:"globals_defs.html#index_e"},
{text:"f",url:"globals_defs.html#index_f"},
{text:"g",url:"globals_defs.html#index_g"},
{text:"h",url:"globals_defs.html#index_h"},
{text:"i",url:"globals_defs.html#index_i"},
{text:"l",url:"globals_defs.html#index_l"},
{text:"m",url:"globals_defs.html#index_m"},
{text:"n",url:"globals_defs.html#index_n"},
{text:"p",url:"globals_defs.html#index_p"},
{text:"r",url:"globals_defs.html#index_r"},
{text:"s",url:"globals_defs.html#index_s"},
{text:"t",url:"globals_defs.html#index_t"},
{text:"u",url:"globals_defs.html#index_u"},
{text:"v",url:"globals_defs.html#index_v"},
{text:"w",url:"globals_defs.html#index_w"}]}]}]}]}
