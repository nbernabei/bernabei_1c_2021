var searchData=
[
  ['showresult',['ShowResult',['../aplicacion__proyecto__tres_8c.html#acba64ef3377e2ecb9393c76206288196',1,'aplicacion_proyecto_tres.c']]],
  ['stopcount',['StopCount',['../aplicacion__proyecto__tres_8c.html#a5145a75c91a83c1c09ac0a0abc1760b2',1,'aplicacion_proyecto_tres.c']]],
  ['switch1int',['Switch1Int',['../switch_8c.html#a2e74731a30702d3acc7c0b0f7f41395f',1,'switch.c']]],
  ['switch2int',['Switch2Int',['../switch_8c.html#a0ad21f1345dc6349ca5e99d081534d91',1,'switch.c']]],
  ['switch3int',['Switch3Int',['../switch_8c.html#aab31211525294a15a34329d7209b9f82',1,'switch.c']]],
  ['switch4int',['Switch4Int',['../switch_8c.html#a1f3c7d2e8c5c4d0523c32c21c9715670',1,'switch.c']]],
  ['switchactivint',['SwitchActivInt',['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t sw, void *ptr_int_func):&#160;switch.c'],['../group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb',1,'SwitchActivInt(uint8_t tec, void *ptrIntFunc):&#160;switch.c']]],
  ['switchesactivgroupint',['SwitchesActivGroupInt',['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t switchs, void *ptr_int_func):&#160;switch.c'],['../group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9',1,'SwitchesActivGroupInt(uint8_t tecs, void *ptrIntFunc):&#160;switch.c']]],
  ['switchesinit',['SwitchesInit',['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c'],['../group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c',1,'SwitchesInit(void):&#160;switch.c']]],
  ['switchesread',['SwitchesRead',['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c'],['../group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262',1,'SwitchesRead(void):&#160;switch.c']]],
  ['sysinit',['SysInit',['../aplicacion__proyecto__tres_8c.html#a599b876befeecfecbda08c7514bd6317',1,'aplicacion_proyecto_tres.c']]]
];
