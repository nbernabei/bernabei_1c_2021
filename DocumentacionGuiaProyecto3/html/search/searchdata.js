var indexSectionsWithContent =
{
  0: "_abcdfghilmnoprstu",
  1: "abdlst",
  2: "cdghilmrstu",
  3: "bchimnops",
  4: "ls",
  5: "ls",
  6: "blmnprst",
  7: "bdls",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Enumerations",
  5: "Enumerator",
  6: "Macros",
  7: "Modules",
  8: "Pages"
};

