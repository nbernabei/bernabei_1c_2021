########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
#####Ejemplo 1: Hola Mundo
###Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

####Ejercicio 7
#PROYECTO_ACTIVO = guia_1_ejercicio_7
#NOMBRE_EJECUTABLE = guia_1_ejercicio_7.exe

####Ejercicio 9
#PROYECTO_ACTIVO = guia_1_ejercicio_9
#NOMBRE_EJECUTABLE = guia_1_ejercicio_9.exe

####Ejercicio 12
#PROYECTO_ACTIVO = guia_1_ejercicio_12
#NOMBRE_EJECUTABLE = guia_1_ejercicio_12.exe

####Ejercicio 14
#PROYECTO_ACTIVO = guia_1_ejercicio_14
#NOMBRE_EJECUTABLE = guia_1_ejercicio_14.exe

####Ejercicio 16
#PROYECTO_ACTIVO = guia_1_ejercicio_16
#NOMBRE_EJECUTABLE = guia_1_ejercicio_16.exe

####Ejercicio 17
#PROYECTO_ACTIVO = guia_1_ejercicio_17
#NOMBRE_EJECUTABLE = guia_1_ejercicio_17.exe

####Ejercicio Integrador A
#PROYECTO_ACTIVO = guia_1_integrador_a
#NOMBRE_EJECUTABLE = guia_1_integrador_a.exe

####Ejercicio Integrador C
#PROYECTO_ACTIVO = guia_1_integrador_c
#NOMBRE_EJECUTABLE = guia_1_integrador_c.exe

###Ejercicio Integrador D
PROYECTO_ACTIVO = guia_1_integrador_d
NOMBRE_EJECUTABLE = guia_1_integrador_d.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
