/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Ignacio Bernabei
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Ejercicio Integrador A
 Realice un función que reciba un puntero a una estructura LED como la que se muestra.*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/
typedef struct leds{
	uint8_t n_led;      /*indica el número de led a controlar*/
	uint8_t n_ciclos;   /*indica la cantidad de ciclos de encendido/apagado*/
	uint8_t periodo;    /*indica el tiempo de cada ciclo*/
	uint8_t mode;       /*ON, OFF, TOGGLE*/
} my_leds_t;

typedef enum {
	ON,
	OFF,
	TOOGLE,
} LED_MODES;

void ControlLeds(my_leds_t *led_controlado);

/*==================[internal functions declaration]=========================*/
void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

void ControlLeds(my_leds_t *led_controlado){
	uint8_t ciclo = 0;
	switch(led_controlado->mode)
	{
	/*Encender led*/
	case ON:
		printf("Led %d encendido", led_controlado->n_led);
	break;
	/*Apagar led*/
	case OFF:
			printf("Led %d apagado", led_controlado->n_led);
	break;
	/*Parpadear led*/
	case TOOGLE:
		while(ciclo<(led_controlado->n_ciclos)){
			printf("Led %d se enciende por %d unidades de tiempo \r\n", led_controlado->n_led, led_controlado->periodo);
			printf("Led %d se apaga por %d unidades de tiempo \r\n\n", led_controlado->n_led, led_controlado->periodo);
			ciclo++;
		}
	break;
	}
}

int main(void)
{
	my_leds_t led = {3, 2, 5, TOOGLE};
	ControlLeds(&led);

	return 0;
}

/*==================[end of file]============================================*/

