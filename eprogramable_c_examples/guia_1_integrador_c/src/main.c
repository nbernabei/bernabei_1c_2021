/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Ignacio Bernabei
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Ejercicio Integrador C
 * Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida
 * y un puntero a un arreglo donde se almacene los n dígitos. La función deberá convertir
 * el dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado
 * como puntero.
 *
 *int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
 *{
 *
 *}
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "math.h"

/*==================[macros and definitions]=================================*/
#define DATA 12345
#define DIGITS 5

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number);

/*==================[internal functions declaration]=========================*/
int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number){

	uint8_t i;
	uint32_t aux = 0;
	for(i=0; i<digits; i++){
		aux = ( pow(10, digits-i-1) );
		bcd_number[i] = data/aux;
		data = data%aux;
		printf("aux: %d data: %d \r\n", aux, data);
	}

	return bcd_number;
}

void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
	uint8_t *valor_bcd;
	BinaryToBcd(DATA, DIGITS, valor_bcd);

    printf("Número separado en dígitos %d-%d-%d-%d-%d", valor_bcd[0], valor_bcd[1], valor_bcd[2], valor_bcd[3], valor_bcd[4]);

   /* uint8_t i;
	printf("Los digitos de data en BCD son: ");
	for(i=0; i<DIGITS; i++){
		if(i == (DIGITS-1)){
			printBits(sizeof(valor_bcd[i]), valor_bcd[i]);
			printf("\r\n");
		}
		printBits(sizeof(valor_bcd[i]), valor_bcd[i]);
		printf(" - ");
	}*/


	return 0;
}

/*==================[end of file]============================================*/

