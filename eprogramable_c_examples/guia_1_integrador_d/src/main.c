/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Ignacio Bernabei
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Guia 1 Integrador D
 * Escribir una función que reciba como parámetro un dígito BCD y un vector de
 * estructuras del tipo gpioConf_t
 *
 * typedef struct
 * {
 * 	uint8_t port; /*!<GPIO port number *
 * 	uint8_t pin; /*!<GPIO pin number *
 * 	uint8_t dir; /*!<GPIO direction '0' IN; '1' OUT
 * 	} gpioConf_t
 *
 * Defina un vector que mapee los bits de la siguiente manera:
 * b0 -> puerto 1.4
 * b1 -> puerto 1.5
 * b2 -> puerto 1.6
 * b3 -> puerto 2.15
 *
 * La función deberá establecer en qupe valor colocar cada bit del dígito BCD
 */




/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "math.h"

/*==================[macros and definitions]=================================*/

typedef struct
  {
  uint8_t port; /*!<GPIO port number */
  uint8_t pin; /*!<GPIO pin number */
  uint8_t dir; /*!<GPIO direction '0' IN; '1' OUT */
  } gpioConf_t;

void printvalorBCD(uint8_t digito, gpioConf_t *puerto){

	uint8_t MASK = 1;
	uint8_t i;
	for(i=0; i<4; i++){
		if(digito&MASK){
			printf("El puerto %d pin número %d está en 1 \r\n", puerto[i].port, puerto[i].pin);
			MASK = MASK << 1;
		} else{
			printf("El puerto %d pin número %d está en 0 \r\n", puerto[i].port, puerto[i].pin);
			MASK = MASK << 1;
		  }
	  }

}

/*==================[internal functions declaration]=========================*/

int main(void)
{
    gpioConf_t conexion[4];
    conexion[0].port = 1;
    conexion[0].pin = 4;
    conexion[0].dir = 1;

    conexion[1].port = 1;
    conexion[1].pin = 5;
	conexion[1].dir = 1;

    conexion[2].port = 1;
    conexion[2].pin = 6;
	conexion[2].dir = 1;

    conexion[3].port = 2;
    conexion[3].pin = 14;
	conexion[3].dir = 1;

	uint8_t digit = 9;

	printvalorBCD(digit, &conexion);

	return 0;
}

/*==================[end of file]============================================*/

