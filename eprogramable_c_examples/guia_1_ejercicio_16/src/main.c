/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor/es:
 * Nicolas Ignacio Bernabei
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Ejercicio 16
 * Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 * Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
 * cargue cada uno de los bytes de la variable de 32 bits.
 *
 * Realice el mismo ejercicio, utilizando la definición de una “union”
*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/
union bits{
	struct{
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	}cada_byte_t;
	uint32_t suma_bytes;
} total_bytes;

/*==================[internal functions declaration]=========================*/
void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}


int main(void)
{	/*Primera parte*/
    uint32_t total = 0x01020304;
    uint8_t byte_1;
    uint8_t byte_2;
    uint8_t byte_3;
    uint8_t byte_4;

    byte_1 = total;
    total = total>>8;

    byte_2 = total;
    total = total>>8;

    byte_3 = total;
    total = total>>8;

    byte_4 = total;

    printf("Byte 1: %x  Byte 2: %x  Byte 3: %x  Byte 4: %x \n ", byte_1, byte_2, byte_3, byte_4);

    /*Segunda parte*/

    total_bytes.suma_bytes = 0x01020304;

    printf("Byte 1: %x  Byte 2: %x  Byte 3: %x  Byte 4: %x \n ", total_bytes.cada_byte_t.byte1, total_bytes.cada_byte_t.byte2, total_bytes.cada_byte_t.byte3, total_bytes.cada_byte_t.byte4);


	return 0;
}

/*==================[end of file]============================================*/

